module Main exposing (..)

import Html exposing (Html, program)
import Widget


--main


main : Program Never AppModel Msg
main =
    Html.program
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }



--model


type alias AppModel =
    { widgetModel : Widget.Model
    }


init : ( AppModel, Cmd Msg )
init =
    ( initialModel, Cmd.none )


initialModel : AppModel
initialModel =
    { widgetModel = Widget.initialModel
    }



--msg


type Msg
    = WidgetMsg Widget.Msg



--view


view : AppModel -> Html Msg
view model =
    Html.main_ []
        [ Html.div []
            [ Html.map WidgetMsg (Widget.view model.widgetModel)
            ]
        ]



--update


update : Msg -> AppModel -> ( AppModel, Cmd Msg )
update msg model =
    case msg of
        WidgetMsg subMsg ->
            let
                ( updatedWidgetModel, widgetCmd ) =
                    Widget.update subMsg model.widgetModel
            in
            ( { model | widgetModel = updatedWidgetModel }
            , Cmd.map WidgetMsg widgetCmd
            )



--subscriptions


subscriptions : AppModel -> Sub Msg
subscriptions model =
    Sub.none
