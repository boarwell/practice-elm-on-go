module Widget exposing (..)

import Html exposing (Html, button, div, main_, text)
import Html.Events exposing (onClick)


--model


type alias Model =
    { count : Int
    }


initialModel : Model
initialModel =
    { count = 0
    }



--msg


type Msg
    = Increase



--view


view : Model -> Html Msg
view model =
    main_ []
        [ div [] [ Html.p [] [ text (toString model.count) ] ]
        , button [ onClick Increase ] [ text "Click" ]
        ]



--update


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Increase ->
            -- OCamlのrecordなら
            -- {<record> with <field> = <value>; <field> = <value>}
            ( { model | count = model.count + 1 }, Cmd.none )
