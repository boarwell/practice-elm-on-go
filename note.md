# note

## [Go] 標準ライブラリだけで静的HTMLを描画する

### http.FileServerを使う

`http.FileServer`で`http.Handler`を作って、それを`http.Handle`に渡す

```go

fs := http.FileServer(http.Dir("."))
http.Handle("/", fs)

```

### http.ServeFileを使う

HundleFuncとして小さな関数に分割しておけるのがメリット？

```go

http.HandleFunc("/", index)

func index(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, "index.html")
}

```


