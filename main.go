package main

import (
	"log"
	"net/http"
)

const (
	port = "8000"
)

func main() {
	// note.mdの「静的HTMLを描画する」を参照せよ
	// fs := http.FileServer(http.Dir("."))
	// http.Handle("/", fs)
	http.HandleFunc("/toggle", toggle)
	http.HandleFunc("/inc-dec", incDec)
	http.HandleFunc("/subsc", subsc)
	http.HandleFunc("/gen-rand", genRand)

	log.Println("DEBUG: listen on", port)
	http.ListenAndServe(":"+port, nil)
}

func toggle(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, "toggle.html")
}

func incDec(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, "inc-dec.html")
}

func subsc(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, "subsc.html")
}

func genRand(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, "gen-rand.html")
}
